# LC-name-authority-extractor

This is a small application that uses named entity recognition (NER) to extract named entities from free text, then uses the Library of Congress linked data service to see if there are authority records matching those entities.  Since many authorities can share a name, there is a basic checklist GUI provided to expedite this process.  The authority records can then be downloaded as MARCXML files, suitable for upload into an information system such as Archivesspace.

More documentation will be coming soon, but for now, to host the application there are a few main steps:

The Pipfile should contain all of the needed dependencies except for the following:

1. A Redis server needs to be running, the application uses the default port

2.  Follow these instructions to obtain a trained model for SpaCy: https://spacy.io/usage/models

There are two Flask application services `front_end` and `back_end`, so these each need to run on their own port so they can talk to each other.

That's about it, it's a pretty simple app and it makes life a lot easier if you are doing archival description for collections that include a good quantity of authors and other scholarly folks, who tend to be included in the LC name authority file.
