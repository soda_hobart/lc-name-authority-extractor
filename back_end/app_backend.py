from time import sleep
from io import BytesIO
import spacy
import requests
from lxml import etree
import json

class MarcHandler:
    @staticmethod
    def str_repr(marcxml_str):
        output_string = str()
        marcxml = etree.fromstring(marcxml_str)
        for element in marcxml.iter():
            if element.tag == "{http://www.loc.gov/MARC21/slim}datafield":
                tag = element.get("tag")
                ind1 = element.get("ind1").replace(" ", "#")
                ind2 = element.get("ind2").replace(" ", "#")
                tagstr = "\n{} {}{}: ".format(tag, ind1, ind2)
                output_string += tagstr
            elif element.tag == "{http://www.loc.gov/MARC21/slim}controlfield":
                tag = element.get("tag")
                text = element.text
                tagstr = "\n{}: {}".format(tag, text)
                output_string += tagstr
            elif element.tag == "{http://www.loc.gov/MARC21/slim}subfield":
                code = element.get("code")
                text = element.text
                tagstr = "${} {}.".format(code, text)
                output_string += tagstr
        return output_string

    @staticmethod
    def get_001(marcxml):
        rec = BytesIO(marcxml.encode('utf-8'))
        for e, i in etree.iterparse(rec, tag="{http://www.loc.gov/MARC21/slim}controlfield"):
            if i.get("tag") == "001":
                return i.text.replace(" ", "")

    @staticmethod
    def write_record_files(recordxml):
        for i in recordxml.iter("{http://www.loc.gov/MARC21/slim}controlfield"):
            if i.get("tag") == "001":
                fname = i.text.replace(" ", "")
                with open("{}.xml".format(fname), "w") as f:
                    output_etree = etree.ElementTree(recordxml)
                    f.write(etree.tostring(output_etree, encoding="UTF-8").decode())
                    print("MARCXML file written for {}".format(fname))
            else:
                print("Missing LCCN for filename")
        

class SpacyNER:
    """Extracts NEs from the text"""
    def __init__(self):
        self.nlp = spacy.load("xx_ent_wiki_sm")
        self.named_entities = dict()
        self.name_authorities = dict()

    def is_propn(self, span, entity_type='PER', *args):
        for token in span:
            if token.ent_type_ != entity_type:
                #print("span: {} token: {} type: {}".format(span, token, token.ent_type_))
                return False
        return True

    def propn_type(self, span, entity_type):
        for token in span:
            pass


    def num_records(self, record_str):
        rec = BytesIO(record_str.encode('utf-8'))
        for e, i in etree.iterparse(rec, tag="{http://www.loc.gov/zing/srw/}numberOfRecords"):
            if i is not None:
                return int(i.text)
            i.clear()

    def extract_from_txt(self, textfile_path):
        with open(textfile_path) as f:
            text = f.read()
            doc = self.nlp(text)
            for span in doc.ents:
                if self.is_propn(span):
                    entity = {'type': 'PER'}
                    self.named_entities[span.text] = entity
                elif self.is_propn(span, entity_type='ORG'):
                    entity = {'type': 'ORG'}
                    self.named_entities[span.text] = entity
            #print(self.named_entities)
            return self.named_entities

    def extract_from_str(self, string):
        #self.named_entities.clear()
        named_entities = dict()
        doc = self.nlp(string)
        for span in doc.ents:
            if self.is_propn(span):
                entity = {'type': 'PER'}
                named_entities[span.text] = entity
            elif self.is_propn(span, entity_type='ORG'):
                entity = {'type': 'ORG'}
                named_entities[span.text] = entity
        return named_entities
        

class MADSRDF:
    def __init__(self, schema):
        self.classes = dict()
        try:
            for i in schema:
                if "http://www.w3.org/2002/07/owl#Class" in i['@type']:
                    self.classes[i['@id']] = i
        except KeyError:
            print('no @type, exception ignored')

    @classmethod
    def make_MADSRDF(cls):
        mads_json = None
        with open("MADS.json", "r") as f:
            mads_json = json.loads(f.read())
        mads_tools = cls(mads_json)
        return mads_tools

class LDClient:
    def __init__(self, ner_instance):
        self.ner = ner_instance
        self.url = 'http://id.loc.gov/search/'

    def build_query(self, queries_list, search_params):
        search_params['q'] = queries_list
        print(search_params)
        return search_params

    def search(self, search_query):
        try:
            r = requests.get(self.url, params=search_query)
        except Exception:
            print('EXCEPTING!!')
            sleep(5)
            r = requests.get(self.url, params=search_query)
        return r.text

    def check_exists(self, search_results):
        open_search_ns = "{http://a9.com/-/spec/opensearch/1.1/}"
        total_tag = "totalResults"
        data_stream = BytesIO(search_results.encode('utf-8'))
        for e, i in etree.iterparse(data_stream, tag=open_search_ns + total_tag):
            try:
                if int(i.text) > 0:
                    return True
                else:
                    return False
            except TypeError:
                print("TypeError ignored, returning False")
                return False

    def extract_URIs(self, atom_data):
        urls = []
        atom_xmlns = "{http://www.w3.org/2005/Atom}"
        link_tag = atom_xmlns + "link"
        data_stream = BytesIO(atom_data.encode('utf-8'))
        for e, i in etree.iterparse(data_stream, tag=link_tag):
            if i.get("type") == None and i.get("rel") == 'alternate':
                urls.append(i.get("href"))
        return urls
    
    def retrieve(self, search_results, record_format=".marcxml.xml"):
        authority_records = dict()
        for i in self.extract_URIs(search_results):
            print(i + record_format)
            r = requests.get(i + record_format)
            lccn = MarcHandler.get_001(r.text)
            authority_records[lccn] = {'marcxml': r.text}
        return authority_records
        
    
class SRUClient:
    def __init__(self, ner_instance, madsrdf=True):
        self.ner = ner_instance
        self.payload = {
            'operation': 'searchRetrieve',
            'version': 1.1,
            'maximumRecords': 100,
            'recordSchema': 'marcxml'
        }
        self.madsrdf = None
        if madsrdf == True:
            self.madsrdf = MADSRDF.make_MADSRDF()

    def is_mads_instance(self, mads_class, madsrdf_str):
        m_class = mads_class
        rec = BytesIO(madsrdf_str.encode('utf-8'))
        for e, i in etree.iterparse(rec, tag=m_class):
            if i is not None:
                return True
            else:
                return False

    
    def loc_sru(self, named_entities, ent_type_check=False):
        name_authorities = dict()
        payload = self.payload
        for name, value in named_entities.items():
            print(name)
            if named_entities[name]['type'] == 'PER':
                payload['query'] = 'bath.personalName={}'.format(name)
            elif named_entities[name]['type'] == 'ORG':
                payload['query'] = 'bath.corporateName={}'.format(name)
            else:
                print('PER or ORG needed')
            r = requests.get('http://lx2.loc.gov:210/NAF', params=payload)
            if r:
                lccn = MarcHandler.get_001(r.text)
                if ent_type_check:
                    ld_file = requests.get("http://id.loc.gov/authorities/names/{}.rdf".format(lccn)).text
                    if self.is_mads_instance("{http://www.loc.gov/mads/rdf/v1#}PersonalName", ld_file) or self.is_mads_instance("{http://www.loc.gov/mads/rdf/v1#}CorporateName", ld_file):
                        record = etree.fromstring(r.text)
                        marcxml = [etree.ElementTree(i)for i in record.iter("{http://www.loc.gov/MARC21/slim}record")]
                        marcxml_strlist = [etree.tostring(i, encoding='UTF-8').decode() for i in marcxml]
                        if marcxml:
                            name_authorities[name] = marcxml_strlist
                    else:
                        print('Item not madsrdf:PersonalName or madsrdf:CorporateName')
                else:
                    record = etree.fromstring(r.text)
                    marcxml = [etree.ElementTree(i)for i in record.iter("{http://www.loc.gov/MARC21/slim}record")]
                    marcxml_strlist = [etree.tostring(i, encoding='UTF-8').decode() for i in marcxml]
                    if marcxml:
                        name_authorities[name] = marcxml_strlist
        return name_authorities


    def quick_sru(self, named_entities):
        """checks to see if NE is in LCNAF"""
        payload = self.payload
        lcnaf_nameset = set()
        for name, value in named_entities.items():
            print(name)
            if named_entities[name]['type'] == 'PER':
                payload['query'] = 'bath.personalName={}'.format(name)
            elif named_entities[name]['type'] == 'ORG':
                payload['query'] = 'bath.corporateName={}'.format(name)
            else:
                print('PER or ORG needed')
            #payload['query'] = 'bath.Name={}'.format(name)
            r = requests.get('http://lx2.loc.gov:210/NAF', params=payload)
            if r:
                lccn = MarcHandler.get_001(r.text)
                print(lccn)
                ld_file = requests.get("http://id.loc.gov/authorities/names/{}.rdf".format(lccn)).text
                if self.is_mads_instance("{http://www.loc.gov/mads/rdf/v1#}PersonalName", ld_file) or self.is_mads_instance("{http://www.loc.gov/mads/rdf/v1#}CorporateName", ld_file):
                    print('true')
                    # why does this space show up at the end of the string?
                    # i don't know...
                    if name[-1] == " ":
                        name = name[:-1:]
                    lcnaf_nameset.add(name)
        lcnaf_names = [i for i in lcnaf_nameset]
        return lcnaf_names
    # need to do some research to compare the check-first method vs. just getting
    # all the data in one big lump.  i think the big lump would probably be more
    # effective for small collections, which is more in tune with the workflow style.


class MARCXML:
    def __init__(self, lccn, str_data, str_repr):
        self.lccn = lccn
        self.str_repr = str_repr
        self.str_data = str_data

    @classmethod
    def make(cls, marcxml):
        lccn = MarcHandler.get_001(marcxml)
        e = etree.fromstring(marcxml)
        str_repr = MarcHandler.str_repr(e)
        return cls(lccn, marcxml, str_repr)
                
            


if __name__ == "__main__":
    ner = SpacyNER()
    sru = SRUClient(ner)
    sru.ner.extract_from_txt("test1.txt")
    sru.loc_sru()
