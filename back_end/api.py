from time import sleep
from flask import current_app, Blueprint, request, jsonify, session
import json
import os
from io import BytesIO
from back_end import redis_server, ner, sru, ld_client
import uuid
from back_end.app_backend import MARCXML, MarcHandler
from flask_cors import CORS


api = Blueprint('api', __name__)
CORS(api)

def with_session(func):
    def inner():
        session_id = None
        if session.new == True:
            session['session_id'] = uuid.uuid4()
            session_id = session['session_id']
        else:
            try:
                session_id = session['session_id']
            except KeyError:
                session['session_id'] = uuid.uuid4()
                session_id = session['session_id']
        print(session_id)
        return func(session_id=session_id)
    return inner


# for frontend, not backend--backend is a whole nother service,
# it doesn't know about the session--it needs to be supplied with
# a session id
def with_session(func):
    def inner():
        session_id = None
        if 'session_id' in session:
            session_id = session['session_id']
        return func
    return inner

@api.route('/textfile', methods=['POST'])
def textfile():
    print(request.files)
    f = request.files['txt_file']
    print(type(f))
    s = request.form['text_str']
    text_str = f.read().decode('utf-8')
    text_str += "\n{}".format(s)
    print(text_str)
    index_key = int(redis_server.lrange("textfile_key", -1, -1)[0])
    index_key += 1
    redis_server.rpush("textfile_key", index_key)
    redis_server.hset("textfile", index_key, text_str)
    response = {str(index_key): redis_server.hget("textfile", index_key)}
    #return json.dumps(response)
    return jsonify(response)

@api.route('/textfile/<file_id>', methods=['GET'])
def get_textfile(file_id):
    response_text = redis_server.hget("textfile", file_id)
    return response_text

@api.route('ner_extractor_sru', methods=['GET'])
@with_session
def ner_extractor_sru(session_id=None):
    index_key = request.args.get('textfile_key')
    text = redis_server.hget("textfile", index_key)
    print(text)
    result = ner.extract_from_str(text)
    auth_names = sru.quick_sru(result)
    response_data = dict()
    for k, v in result.items():
        if k in auth_names:
            entity_mapping = {'name': k, 'type': result[k]['type']}
            session_k = session_id + k
            redis_server.sadd('session', session_k)
            hash_k = "{}:{}".format('session', session_k)
            redis_server.hmset(hash_k, entity_mapping)
            response_data[hash_k] = entity_mapping
    return jsonify(response_data)

def default_query_rules(entity_name, entity_type=None):
    """helper func for ner_extractor(), ner_result_obj must be dict"""
    params = dict()
    query = []
    if entity_type == "PER":
        query.append("rdftype:PersonalName")
    elif entity_type == "ORG":
        query.append("rdftype:CorporateName")
    else:
        print("No entity type attached")
    query.append(entity_name)
    params['format'] = 'atom'
    search_query = ld_client.build_query(query, params)
    return search_query

def calc_percent(num, total):
    percentage_done = num/total
    percentage_done *= 100
    percentage_done -= percentage_done % 1
    return percentage_done

def track_progress(progress_pos, session_id):
    progress_key = "{}:{}".format(session_id, "operation_progress")
    redis_server.set(progress_key, int(progress_pos))

@api.route('ner_extractor', methods=['GET'])
def ner_extractor():
    if request.args.get('session_id'):
        session_id = request.args.get('session_id')
    else:
        session_id = str(uuid.uuid4())
    index_key = request.args.get('textfile_key')
    text = redis_server.hget("textfile", index_key)
    result = ner.extract_from_str(text)
    print(result)
    names_total = len(result.keys())
    names_pos = 0
    track_progress(0, session_id)#zeroing tracker
    auth_names = dict()
    unauth_names = set()
    for k in result.keys():
        if k not in auth_names.keys() and k not in unauth_names:
            entity_type = result[k]['type']
            search_query = default_query_rules(k, entity_type)
            search_results = ld_client.search(search_query)
            if ld_client.check_exists(search_results):
                entity_mapping = {'name': k,
                                  'type': entity_type,
                                  # a less than ideal solution--
                                  # the nested json is annoying :|
                                  'query': json.dumps(search_query),
                                  'session': session_id
                }
                key_w_session = session_id + '_' + k
                redis_server.sadd('session', key_w_session)
                redis_server.hmset(key_w_session, entity_mapping)
                auth_names[k] = entity_mapping
            else:
                unauth_names.add(k)
                print("No authorized record found")
        names_pos += 1
        progress = calc_percent(names_pos, names_total)
        track_progress(progress, session_id)
    track_progress(0, session_id)#zeroing out the progress tracker
    return jsonify(auth_names)

#TODO: cache LCCN hash in redis to eliminate the number of
# times serialized data is sent back & forth  
@api.route('search_loc_ld', methods=['GET'])
def loc_id_search():
    """searches id.loc.gov for selected items"""
    if request.args.get('session_id'):
        session_id = request.args.get('session_id')
        print("THE SESSION ID IS: {}".format(session_id))
    else:
        session_id = str(uuid.uuid4())
        print("YOU DON'T GOT THE SESSION ID DUMMIE!!!!!")
    named_entities = json.loads(request.args.get('named_entities'))
    store = request.args.get('store_results')
    if store:
        session_hm = dict()
    #progress tracking
    names_total = len(named_entities.keys())
    names_pos = 0
    track_progress(0, session_id)#zeroing tracker
    #end progress tracking
    name_authorities = dict()
    for k in named_entities.keys():
        try:
            q = json.loads(named_entities[k]['query'])
        except TypeError:
            print('TypeError excepted, JSON formated properly')
            q = named_entities[k]['query']
        print(q)
        if q:
            search = ld_client.search(q)
            #print(search)
            records = ld_client.retrieve(search)
            name_authorities[k] = records
            #TODO: encapsulate these transformations in
            # MARCXML object in app_backend.py
            for record in name_authorities[k].keys():
                rec = name_authorities[k][record]
                if rec:
                    #if store:
                       # session_id = named_entities[k]['session']
                       # session_hm[record] = rec['marcxml']
                       # redis_server.hmset(session_id, session_hm)
                    str_repr = MarcHandler.str_repr(rec['marcxml'])
                    rec['marc_text_repr'] = str_repr
                else:
                    print('Null record for id {}'.format(record))
        else:
            print("Query not found")
        names_pos += 1
        progress = calc_percent(names_pos, names_total)
        track_progress(progress, session_id)
    # could be useful to put results in session store and/or cache
    #flask.jsonify returns some of TypeError
    track_progress(0, session_id)#zeroing tracker
    return json.dumps(name_authorities)
    

@api.route('/get_progress')
def get_progress():
    session_id = request.args.get('session_id')
    print(session_id)
    progress_key = "{}:{}".format(session_id, "operation_progress")
    print(progress_key)
    progress = redis_server.get(progress_key)
    print(progress)
    print(type(progress))
    return progress
    

#currently broken!
@api.route('lcnaf_sru', methods=['GET', 'POST'])
def lcnaf_sru():
    named_entities_id = request.args.get('named_entities_id')
    named_entities = redis_server.lrange(named_entities_id, 0, -1)
    name_authorities = sru.loc_sru(named_entities)
    return json.dumps(name_authorities)

# there could be a POST method of this that writes to the common
# data store... but do we really care because it is not fully
# persistent data anyways?
@api.route('lcnaf_sru_selected', methods=['GET'])
def lcnaf_sru_selected():
    """Does SRU query for named entities, args are json data and session_id"""
    named_entities = json.loads(request.args.get('named_entities'))
    query_data = dict()
    # random thought: the session store data could be used as a
    # cache if the client side doesn't make any changes--client
    # side could report 'no changes' instead of sending data,
    # triggering use of session store data, & improve performance
    for k, v in named_entities.items():
        name = named_entities[k]['name']
        query_data[name] = named_entities[k]
    name_authorities = sru.loc_sru(query_data, ent_type_check=True)
    response_data = []
    for name, recordset in name_authorities.items():
        for i in recordset:
            marcxml = MARCXML.make(i)
            record_mappings = {
                'lccn': marcxml.lccn,
                'assoc_name': name,
                'str_repr': marcxml.str_repr,
                'str_data': marcxml.str_data
            }
            # skipping writing anything to cache for right now
            response_data.append(record_mappings)
    return jsonify(response_data)
    
    

@api.route('lcnaf_sru_selected_old', methods=['GET'])
def lcnaf_sru_selected_old():
    named_entities = json.loads(request.args.get('named_entities'))
    new_entities = []
    for name in named_entities:
        if not redis_server.sismember("authorized_entities", name):
            new_entities.append(name)
    name_authorities = sru.loc_sru(new_entities)
    for name, recordset in name_authorities.items():
        for i in recordset:
            marcxml = MARCXML.make(i)
            record_mappings = {
                'assoc_name': name,
                'str_repr': marcxml.str_repr,
                'str_data': marcxml.str_data
            }
            redis_server.hmset(marcxml.lccn, record_mappings)
            redis_server.rpush(name, marcxml.lccn)
            redis_server.sadd("authorized_entities", name)
    response = []
    for name in named_entities:
        identifiers = redis_server.lrange(name, 0, -1)
        for i in identifiers:
            auth_rec = redis_server.hmget(i, "assoc_name", "str_repr", "str_data")
            response_record = {
                "lccn": i,
                "assoc_name": auth_rec[0],
                "str_repr": auth_rec[1],
                "str_data": auth_rec[2]
            }
            response.append(response_record)
    return jsonify(response)

def init_app(app):
    app.register_blueprint(api, url_prefix='/api')
    
