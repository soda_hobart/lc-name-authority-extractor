from flask import Flask
import redis
from back_end import app_backend
from flask_cors import CORS

ner = app_backend.SpacyNER()
sru = app_backend.SRUClient(ner)
ld_client = app_backend.LDClient(ner)

redis_server = redis.Redis(host='localhost', port=6379, db=0, decode_responses=True)
if not redis_server.lrange("textfile_key", -1, -1):
    redis_server.rpush("textfile_key", 0)

def create_app():
    app = Flask(__name__)
    CORS(app)
    
    from back_end import api
    api.init_app(app)

    return app
