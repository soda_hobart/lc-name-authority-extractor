import os
from flask import Flask, render_template, request, redirect, url_for, send_from_directory, jsonify, current_app, session
from flask_bootstrap import Bootstrap
import socket
import requests
import json
from io import BytesIO
import uuid
import tarfile
import functools

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
try:
    s.connect(('8.8.8.8', 1))
    backend_ip_address = s.getsockname()[0]
except OSError:
    backend_ip_address = 'localhost'

backend_url = "http://{}:5001".format(backend_ip_address)

frontend_url = "http://{}:5000".format(backend_ip_address)

base_dir = "front_end/"

instance_path = os.environ.get('FLASK_APP_INSTANCE_PATH')


def with_session(func):
    @functools.wraps(func)
    def inner():
        session_id = None
        if session.new == True:
            print('new session')
            session['session_id'] = str(uuid.uuid4())
            session_id = session['session_id']
        else:
            print('existing session')
            try:
                session_id = session['session_id']
            except KeyError:
                session['session_id'] = str(uuid.uuid4())
                session_id = session['session_id']
        print(session_id)
        return func(session_id=session_id)
    return inner

def create_app():
    app = Flask(__name__, template_folder='templates', instance_path=instance_path)
    app.config.from_mapping(SECRET_KEY='dev')
    Bootstrap(app)

    
    @app.route('/')
    @with_session
    def hi(session_id=None):
        api_upload = "{}/upload".format(frontend_url)
        return render_template('index.html', api_upload=api_upload, frontend_url=frontend_url)

    
    @app.route('/named_entities/<textfile_id>', methods=['GET', 'POST'])
    def extract_entities(textfile_id, session_id=None):
        session_id = session_id
        if session.new == True:
            print('new session')
            session['session_id'] = str(uuid.uuid4())
            session_id = session['session_id']
        else:
            print('existing session')
            try:
                session_id = session['session_id']
            except KeyError:
                session['session_id'] = str(uuid.uuid4())
                session_id = session['session_id']
        url = "{}/api/ner_extractor".format(backend_url)
        payload = {'textfile_key': textfile_id, 'session_id': session_id}
        r = requests.get(url, params=payload)
        response_data = r.text
        print(response_data)
        print(type(response_data))
        return render_template('named_entities2.html', response_data=response_data, session_id=session_id, backend_url=backend_url, frontend_url=frontend_url)

    @app.route('/extract_authorities')
    def extract_authorities():
        pass

    
    @app.route('/upload', methods=['POST'])
    @with_session
    def upload(session_id=None):
        print("SESSION ID: ".format(session_id))
        url = "{}/api/textfile".format(backend_url)
        #print(request.files)
        #print(request.form['inputTextarea'])
        text_str = request.form['inputTextarea']
        f = BytesIO()
        f.write(request.files['inputFile'].read())
        print(f.getvalue())
        headers = {'enctype': "multipart/form-data"}
        file_payload = {'txt_file': f.getvalue()}
        form_payload = {'text_str': text_str}
        p = requests.post(url, headers=headers, data=form_payload, files=file_payload)
        response = json.loads(p.text)
        textfile_id = [i for i in response.keys()][0]
        print(textfile_id)
        return redirect(url_for('extract_entities', textfile_id=textfile_id, session_id=session_id))

    
    @app.route('/get_progress')
    @with_session
    def get_progress(session_id=None):
        payload = {'session_id': session_id}
        url = "{}{}".format(backend_url, "/api/get_progress")
        r = requests.get(url, params=payload)
        return r.text

    
    @app.route('/compress_records', methods=['POST'])
    @with_session
    def compress_records(session_id=None):
        tar_filename = str(uuid.uuid4()) + '.tar.gz'
        tar_filepath = os.path.join(current_app.instance_path, "records", tar_filename)
        tar = tarfile.open(tar_filepath, "w:gz")
        print(request.form.get('marcxml_records'))
        records = json.loads(request.form.get('marcxml_records'))
        print("records: {}".format(records))
        for record in records:
            print(record)
            rec_filename = record['lccn'] + '.xml'
            rec_filepath = os.path.join(current_app.instance_path, "records", rec_filename)
            marcxml_file = open(rec_filepath, "w")
            #TODO: formalize named entity and named authority
            # object schema so the frontend and backend can
            # have a common frame of reference.
            marcxml_file.write(record['marcxmlStr'])
            marcxml_file.close()
            tar.add(rec_filepath, arcname='{}'.format(rec_filename))
        tar.close()
        response = {'gzip_file': tar_filename}
        return jsonify(response)

    
    @app.route('/download/<filename>', methods=['GET'])
    #@with_session - TODO: figure out the problem with decorators
    # and kwargs
    def download(filename, session_id=None):
        tar_filename = filename
        tar_filepath = os.path.join(current_app.instance_path, "records")
        print(tar_filepath)
        print(tar_filename)
        return send_from_directory(tar_filepath, tar_filename, as_attachment=True)
            

    return app
